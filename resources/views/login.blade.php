<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/adam.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>
    <div class="container">
        <h1 >Sign In</h1>

        <img src="images/undraw_by_my_car_ttge 1.png" alt="" style="margin-top:64px; margin-bottom:64px;">
        <form action="{{ route('checkLogin') }}" method="post">
            @csrf
            <div class="form-group">
              <input type="email" name="email" class="form-control"  aria-describedby="emailHelp" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Kata Sandi">
            </div>
            <button type="submit" class="btn btn-primary">Masuk</button>
        </form>
        <p>Belum memiliki akun? <a href="register">Sign up</a>
        <br><a href="forget-password">Lupa password?</a></p>

    </div>
</body>
</html>
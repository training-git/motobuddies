<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
  config = {
    dateFormat: "Y-m-d H:i",
    enableTime: true,
    time_24hr: true,
    altInput: true,
    altFormat: "d M Y H:i",
    dateFormat: "Y-m-d H:i",
    minDate: "today",
    inline: true
  };

  flatpickr("#flatpickr", config);

  $(function(){
    $('form').submit(function(event){
      let date = JSON.stringify($(this).serializeArray());
      $('#message').text(date);
      event.preventDefault();
    });
  });
</script>
</body>
</html>
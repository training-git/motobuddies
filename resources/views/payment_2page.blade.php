<!DOCTYPE html>

<html lang="english">

<head>
  <title>Lakukan Pembayaran</title>
  <meta property="twitter:card" content="summary_large_image" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta charset="utf-8" />
  <meta property="twitter:card" content="summary_large_image" />
  <style data-tag="reset-style-sheet">
    html {
      line-height: 1.15;
    }

    body {
      margin: 0;
    }

    * {
      box-sizing: border-box;
      border-width: 0;
      border-style: solid;
    }

    p,
    li,
    ul,
    pre,
    div,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    button,
    input,
    optgroup,
    select,
    textarea {
      font-family: inherit;
      font-size: 100%;
      line-height: 1.15;
      margin: 0;
    }

    button,
    select {
      text-transform: none;
    }

    button,
    [type="button"],
    [type="reset"],
    [type="submit"] {
      -webkit-appearance: button;
    }

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
      border-style: none;
      padding: 0;
    }

    button:-moz-focus,
    [type="button"]:-moz-focus,
    [type="reset"]:-moz-focus,
    [type="submit"]:-moz-focus {
      outline: 1px dotted ButtonText;
    }

    a {
      color: inherit;
      text-decoration: inherit;
    }

    input {
      padding: 2px 4px;
    }

    img {
      display: block;
    }
  </style>
  <style data-tag="default-style-sheet">
    html {
      font-family: Inter;
      font-size: 16px;
    }

    body {
      font-weight: 400;
      font-style: normal;
      text-decoration: none;
      text-transform: none;
      letter-spacing: normal;
      line-height: 1.15;
      color: var(--dl-color-gray-black);
      background-color: var(--dl-color-gray-white);

    }
  </style>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
    data-tag="font" />
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    data-tag="font" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap');
  </style>
  <link rel="stylesheet" href="style.css" />
</head>

<body>
  <div>
    <link href="/css/payment_stylesheet.css" rel="stylesheet" />

    <div class="pembayaran-lakukanpembayaran249-container">
      <div class="pembayaran-lakukanpembayaran249-pembayaran-lakukanpembayaran249">
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMzcxJyBoZWlnaHQ9JzM5Nicgdmlld0JveD0nMCAwIDM3MSAzOTYnIGZpbGw9J25vbmUnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+CjxnIGZpbHRlcj0ndXJsKCNmaWx0ZXIwX2RfMl81MCknPgo8cmVjdCB4PSc2JyB5PSc1JyB3aWR0aD0nMzU3JyBoZWlnaHQ9JzM4Micgcng9JzEwJyBmaWxsPSd3aGl0ZScvPgo8L2c+CjxkZWZzPgo8ZmlsdGVyIGlkPSdmaWx0ZXIwX2RfMl81MCcgeD0nMCcgeT0nMCcgd2lkdGg9JzM3MScgaGVpZ2h0PSczOTYnIGZpbHRlclVuaXRzPSd1c2VyU3BhY2VPblVzZScgY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzPSdzUkdCJz4KPGZlRmxvb2QgZmxvb2Qtb3BhY2l0eT0nMCcgcmVzdWx0PSdCYWNrZ3JvdW5kSW1hZ2VGaXgnLz4KPGZlQ29sb3JNYXRyaXggaW49J1NvdXJjZUFscGhhJyB0eXBlPSdtYXRyaXgnIHZhbHVlcz0nMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMTI3IDAnIHJlc3VsdD0naGFyZEFscGhhJy8+CjxmZU9mZnNldCBkeD0nMScgZHk9JzInLz4KPGZlR2F1c3NpYW5CbHVyIHN0ZERldmlhdGlvbj0nMy41Jy8+CjxmZUNvbXBvc2l0ZSBpbjI9J2hhcmRBbHBoYScgb3BlcmF0b3I9J291dCcvPgo8ZmVDb2xvck1hdHJpeCB0eXBlPSdtYXRyaXgnIHZhbHVlcz0nMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMC4xOCAwJy8+CjxmZUJsZW5kIG1vZGU9J25vcm1hbCcgaW4yPSdCYWNrZ3JvdW5kSW1hZ2VGaXgnIHJlc3VsdD0nZWZmZWN0MV9kcm9wU2hhZG93XzJfNTAnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbj0nU291cmNlR3JhcGhpYycgaW4yPSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl81MCcgcmVzdWx0PSdzaGFwZScvPgo8L2ZpbHRlcj4KPC9kZWZzPgo8L3N2Zz4K"
          alt="Rectangle245250" class="pembayaran-lakukanpembayaran249-image" />
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDI4JyBoZWlnaHQ9JzEwMScgdmlld0JveD0nMCAwIDQyOCAxMDEnIGZpbGw9J25vbmUnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+CjxyZWN0IHg9Jy0xJyB3aWR0aD0nNDI5JyBoZWlnaHQ9JzEwMScgZmlsbD0nIzIyMjIyMicvPgo8L3N2Zz4K"
          alt="Rectangle172251" class="pembayaran-lakukanpembayaran249-image1" />
        <div class="pembayaran-lakukanpembayaran249-group1179256">
          <img alt="Ellipse9257"
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDInIGhlaWdodD0nNDQnIHZpZXdCb3g9JzAgMCA0MiA0NCcgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPGVsbGlwc2UgY3g9JzIxJyBjeT0nMjInIHJ4PScyMScgcnk9JzIyJyBmaWxsPScjRkY3QTQ1Jy8+Cjwvc3ZnPgo="
            class="pembayaran-lakukanpembayaran249-svg" />
          <img
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTYnIGhlaWdodD0nMTgnIHZpZXdCb3g9JzAgMCAxNiAxOCcgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPHBhdGggZD0nTTE0LjI5OTkgMTYuMzMzM1YxNC43MDM3QzE0LjI5OTkgMTMuODM5MiAxMy45NjggMTMuMDEwMiAxMy4zNzczIDEyLjM5OUMxMi43ODY1IDExLjc4NzggMTEuOTg1MyAxMS40NDQ0IDExLjE0OTkgMTEuNDQ0NEg0Ljg0OTlDNC4wMTQ0NyAxMS40NDQ0IDMuMjEzMjUgMTEuNzg3OCAyLjYyMjUxIDEyLjM5OUMyLjAzMTc3IDEzLjAxMDIgMS42OTk5IDEzLjgzOTIgMS42OTk5IDE0LjcwMzdWMTYuMzMzMycgc3Ryb2tlPScjRjlGOUY5JyBzdHJva2Utd2lkdGg9JzInIHN0cm9rZS1saW5lY2FwPSdyb3VuZCcgc3Ryb2tlLWxpbmVqb2luPSdyb3VuZCcvPgo8cGF0aCBkPSdNNy45OTk5OSA4LjE4NTE0QzkuNzM5NjkgOC4xODUxNCAxMS4xNSA2LjcyNTkyIDExLjE1IDQuOTI1ODlDMTEuMTUgMy4xMjU4NSA5LjczOTY5IDEuNjY2NjMgNy45OTk5OSAxLjY2NjYzQzYuMjYwMjkgMS42NjY2MyA0Ljg0OTk5IDMuMTI1ODUgNC44NDk5OSA0LjkyNTg5QzQuODQ5OTkgNi43MjU5MiA2LjI2MDI5IDguMTg1MTQgNy45OTk5OSA4LjE4NTE0Wicgc3Ryb2tlPScjRjlGOUY5JyBzdHJva2Utd2lkdGg9JzInIHN0cm9rZS1saW5lY2FwPSdyb3VuZCcgc3Ryb2tlLWxpbmVqb2luPSdyb3VuZCcvPgo8L3N2Zz4K"
            alt="Group50258" class="pembayaran-lakukanpembayaran249-image2" />
        </div>
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDI4JyBoZWlnaHQ9JzMwJyB2aWV3Qm94PScwIDAgNDI4IDMwJyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8cmVjdCB4PSctMScgd2lkdGg9JzQyOScgaGVpZ2h0PSczMCcgZmlsbD0nI0ZGN0E0NScvPgo8L3N2Zz4K"
          alt="Rectangle173262" class="pembayaran-lakukanpembayaran249-image3" />
        <span class="pembayaran-lakukanpembayaran249-text">
          <span>Langkah 2/3</span>
          <br />
          <span>Lakukan Pembayaran</span>
        </span>
        <img
          src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALAAAAAlCAYAAADiHGOuAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABDmSURBVHgB7ZwLuBxlecf/s3uuCTkJTUASEpKC5oqQFijWWkTBahFyU6JAawF9Kg3eSmmrFeipFtuGFluKSCvV0GKLEQKkmlovJDQKBVEjgSSSSAIRhAROLiQ5lz274/vbmX3Onjnf7MzsboL6nP/DS3ZnZr+5fO/3vv/3MsdTVqz+0SSVSjNV0gXy/dPl6QTbOsVkrIln/w3IK+2Rn9tm3zfJ976u8W3r1fPoi1q6tKhRjKKJ8FId5fueVm1GUZeZLLFfvVpZ4Ot55fz7lcvfpp5ZD+kyr0+jGEUTkKzAX91+nAb6rjYlvsy+/Yoaga8+ef5atXZ8WBectFWjGEWDiFfglSvzannt75ri3mRH/aqaC1Nk72+VP3iDFpx+SKMYRZ1wK/CaNe3qn36V8ddu+9amw3JmzzeLfLfy7R/QwhNf0ChGUQdGKvCare3qK1xrnz6uIwHf/45axi3VwhOeU3PBwptvcrLJVJNOBfe72+T7Jv9v0hseu8Dktx1j/IvJNqVDq8lrTeaG5+sSpEl62WS7yaPhv6WU4+XlNjDFcFww0eQNJicqoHcFk+dNNpg8btKId+P6TzPB+xKkd4TnflHBM/megmfpK/21R68/LVpitvsjT7Jq83XySx8zC9mhIwVfD6p97Pk6f/oeNY52k0tN3qdgYscreKDVIIiEg99u8lmTvzP5gGOs15k8rNo4xuSPTBabTDcZp5EPHMXaZ/JDkztM/sukP2Hca0wuc2xnse03+VOT800mK1CuCvxw7KfC83zGJMtznWfyYZM3m0xSoMhRPeF+9pp81+QfTO6P7H+Tyc2R6wIoL8bxS0oPDBDzNMGx74bhF7Zqy4WmvHfa5eZ0pOH7q1Wcu0RLvUZSba83+WeTX8/wm80mPzF5i2NfLQVmct6vYEImKhueVJDR+VaNY5YrUNIoWGzvy3BO6NlVJneqtvVnoX9SwT1loY2DChYKSl9ZKC3h+d7hOJ7nebYCI5IEDA8L4QrHvi0mC4csxX1bpmiweKNZ3ljlHZP3NGdcm6Z3tmnG2FYd39GisfmcOk36/JL6Bkt6YaCopw8W9EzvoJ48MKBd/YPp/IWXe7tatlxunz6n7GAhLlFg3bJ6jjmhZAFWloXyHqVNRQ7HTJO7TLoVeIABxzGDMb/9s4znfJXJbSakPj8VMy5W/OsKrF1WoEO/b3KsCfP3XHgOzokljmauoCXvVDBXSeA5LYzZd4/J1kCBV/p5FbcsN+Wd6joSjb5o6nh9Yu4kHWdKOyafbKCLvq+9AyVds3mXbt2+V8mwa/D1CX3xsbt0ySlZqcQFJl9QduWtB5wDq/AeNQZc4qcVuONbHPvj1n09Cwb+/5cKqMc/RvZRgPoP1ae81fgdkys1FDt9Q0GccV7kOHTuUpPV4fXEASVbanKcYx+xBAvfDxS4bdPpKnkXuEZpz3n685kTde2sSWrJpX92ec/TxPa8bpk/Wc/3FXXvT19O/pFnFzum7Tr79MdKj9kmn1dgFaPAZfaYPK0g8CiEx1UCu3ZlB1w5Tnk5349NHgz/JbB7jQJLhIWKPkC+o8SPKAjysgJe/SOTHQoC0qMVPA+sbdTK8B0LvFFD1IVtV4fX58ImBYYBa4eyjVFgQf/Q5K0aeS94Bzg3VhgqyFyeq5GUBH79RpP/VjwIHt8v94L9N5OdfAgU2M/9nv2vyzXKaRM69METj86kvNXgV1fa71MpcPlaigt077YbtejVO1MczfAfk5sPVtwYyk3WoZpbw/fepYCHnqL0Vg0ljMvOEJ1fr+DhRm92moKgDMWPegkmF76LpUpboSRIW6lAWVD+amvNAr1QgTLNivwOS4wSnxteI9kF3LnLpa5RwD2j84Ax+JrJ/yrIgFSjJRz738PvZCq+ooDeVYPnfVV4jriY5yMKqE0UPOebh05Ipa2/950xg+icY8ZoUvsQVd7ZW9BHN+5Sz0D6WOulQpa4zGhMqfA2pePCKMYCx3Ys4QcVKLCL82G5/tVkvQILc6bS4b1yR8NwJAKr+2J+hxIQkO0J/40qzBkK0njfUDLwIqT3WLiuNBmKyaJFee42OSmynwCX+/2mgsXTE15/9X2RimOxvSQ3WGhkOd7g2Hdq5PvfmLxdI70dFvi3TP7PMcYkxXu5mxTMXxk5Ffq5CBfPUIutk1PHDzcYbUYNWs0at2WQybYAWAgEfSlMXZuVm88r918kA47kUigChFsVHwhVQAaCwKOgZKB0SxzbWSxE76sTfo8L/mu5Mw9HmbxN6fCQAreflOMlZXe547gKBwUoIYoE9SB9xgLFun1UQ8rLPKB8eDkWA8qIhb0k5rzR+cD7udJmjAsvd1l/FqeLETypKusb3oz/esWgI5fTlI7hKc1X2fcVp09RPSAjcdO2Hn3a5FCpRm7Ct4e5ejvuOqlC95uObSjUzUoPeB48MimIwZ1Nc2zHun5V6ZLzBxRYxbMV8ONqYLl42EmLbq3SLTiAFWbBROObX3McuykUFIsJPie8JmgIjVzkuLn/o5QNzAepPzxlVLlJU2JAq60w574oZiwCt2EBfotK/hkxB6vd0mYT2/JqFo41S/zJeceqw7IY123eXWPG/eNV6ifISlJgF0fCJW5XNhxIcQxeqtOxfWfG82EZD2rkZGLhyAjsS/h9ymCiDM5DVS6qwK7nxuLBC+BlUCqsbbPqAeRsCQSjhRmCQrg6ueFKYeeKmOt7xuSL0Y0506JZigFUoau1eQoMWN7LLKib2tlS46Bcp6WwJyePNsKKAfhZ2nJtFsClXBPKgx9QeuCaXVaWB92i5sOl8FE+CoUgKCSdhpKROUlSXuhH2qCzYoVdwRDZjPnhZ4zW0pgx/l5B6XoYcpb7jW2R5GmOyzdrEQ7haLPqp4yrkbKF/xZzRysZL7qGV/Z8cBrFQfFcrntqeM60mBhzPrhqGoXIOiHHOrZVJ+YphZNVoBQ+IWaMUvibHQq4/rsVVNmSyuHVgKbd6tjOs7gm/Ay/dhlUKqXOoD5XLiDEwPPqy5pXY/vBAb330ee0dvfBoXFNuloSRvb9ViVji2MbbviNSg9owcwUxz2r4RNfwQyTs5Qe5EAnxIyfpvkmy8sEBEKuLM0zVZ/Juf+G4xiUFtdOgAofJv9LoxKVMYIy8s5Z3TMFmx7HdoogxDNXx/yOfgvn4jYFjn87gjhrMHPj0HAQtH1+5z59LlKNS0ysecU0vJREuOsCiWI7lQ5MYFeK43DFP3Rsxx3/idL1JqB8Fzu2cw8PKV0giMtNQ6/ApQroQBTfDP8lH+4qYuwLz4PiUoxYp4Ay9FYdQ4k6ayGIrM9Kx3bSefROnOTYR+YhtuxsFEI/jds5aI9z/2BjdHLBlK6yxT37mDHDtvcUao47aOa/R8mgG+oJx3baGv9TwUOOA5P3IQXNOGlBkcJ14eRwcXFTa/yWySH3PM+xj8rVXUoHsgFMdi1LjBey4lS5yhd1dUTxt4ef8QQuivFlBUp+UG4Q0P6V3DFILbBAyeO6qN8Jcne9/VPM8WVYYra0Ub7nfOOi4PvlgsXUzqzXOYR5XW26eGqXzpo4pMCDNu7+mgpcVt40Te5YRapYKEaU9y5SYH1ouiGdhFXhRrgQ0jdwOBQvixukckTxw0VR4JBzw/NRHSPgIFiDY1Kdovwap3QrFPDLtICyoGC4ZFJQ0A8CSRQXDomVJxhy8eU7q86FI3RNBPdHYLVRQ86SZzc53EeeeK7qA7SPPPJHlMznOf+9tQ5oMR1/2KbQxZPUV/T1Un9jLxKTOrvjjOOHbTtk4+6tVZ3zrFzpd6QpJQOsBWVY14Rh7QgcUF4oCZMAXai36YfJpgS6Vm7agfLcHJ6vUgggSJ5QY0xyr8uVHVhiIvu94bkIqPAqWMe4Rfnj8FyVLAiLbIdGLiwW/j3h8RzjhePSXz1NjQErvELBfNXyWFhfvGjNFx1alM99xy7Pd1W+eosl/aQ3Ka+eHQdou+yvMa7vb9DiGUn50Arg8KxmOOhbYo4ZH0ozQGWJJpNa3W9pz0dfARO5X/VjgmovkArwVlTPdlRtQ+kprMCDo0o/I5TDAd4WIZuxrMYxUMMvKAE59R18whTG2bTNUnl4T69KjcVxI7Bpf7/2xXPrgoreXeV35tIDugFloEcg6yv7cMLN2X5SdsNEzo+pPnDz9E2w4J7I+NusrwnxHKE0VFxd80zfxO3K/prPA8r+3KqvqVvxNJH99IwkxkE5XXSauR8vrglFX35uv767p1fNxG07arX7eo9pTnGdsoOJxb1j0dYpeUJwAbwKQ+fWBmUHXJi8ZbeC5pc04JrgdQSPdMPV86cFUDa4b5pJwf0TyLG4H485ZiC8HoKyNIuD41coCBIbeRmXa3sqZh8Lbq1SIKAN926bpmLhAXm+M5ibN65Nq86cppnjGn9BGeW9csMLGvDj9Ct3uZbMTnQdCcClktvEStLxBDcleMPy7VDwcOB4ZDGgKrwS827HOHRtPaJkUL9/c3g+AkSeY6VYgZsmib/O5H8U9P2+mGJMWjP/wrGdpD8dXrxFsiSUk6vOhxLSSE7AiZumzJ2GB6ILBGaUnXnXjj6ISt8Dzw1lo+eDNNj3wvvimUU5MQv0a0oGHmGdRmYyUAwC4vuUAkO8974tV6jof4bmYNeBE1py5b7eC4/vKjf00GUWdKWVUxnlokcFlfwxHZeFUkn9pqxPHSjolu17dPezL9dQXn1bi+eclZE+NAPkYF8X2UaUySRmdfGASanwYyxlPYFELQW+PrINyzI+PA98uhl/wgs96AjH7lN2alYLPB8W2LmOfSxyFkaquGCopNk/eIda8rhfZ3f+XuOs1z/5kj5lMr41ZyXmvMa2euWOtTG5kQp8yALAXvtAwLbPMg5kHhK0cp9y3rImKC8PJy495ALW+TTHdqL7el1kQek7xpoB3PpuNRc8v0Nq7NX8OFAgOduxnfuAHqUOaocUeOnJB3T3psvMmOLiJsX9AO3aazlcpIlrsqBc/uNaNGujGgOuHPqxSkFuOGkxkOIi8HMluglQ0rzMN4psgMpRMnb1gxCTrFcGDKcL75j7tHI+nCpNFaw58E15S6Xl2jDzs2oM5DLpfYWHkvtdpyB4IX+JK6x0e1H+pPoEz6IIENc3Qfmy+TnEURAruHpH4NRY37Tp0zJGroJF89Zr9ZYLNViibDpDhxcFowzXqTjvBnV7jdSs6WlG4aqbcs4KZZeCdNeucDvvjPEe3PQa41FcuEevLA5HS+grDYwH1tfl8Qis0wR/w+Au5S2Yfb/x0UuMTtSTXkoLSz35H9JjX1re4B8zgQZQsYnrKMPaEixcHApRdi3lhRjxevguvbL4ZbT+NMy7/oQXVO9G1RE3xNeiF815UIWB8+Xl4v7wRv3wjOfkW8/Tknm3qru7UUsD3SGP+YwaB8rLa/MPaBTNBrp2rdw6R9rvW6oDtZspls5/VotnL1Nn7gxz9V+xit1B1Q84zgY74x+o8PibtPA1P1DzQOqFTAIBXL3RP9ezOBzjSKfxXOA+DjnkF9Uy069C7jp6P3BeUoN1GbL0/erd3TnNv9i4pv8uFUvnWNGDi0loUzNe6/m7TfHXyy+t0FGldXrrqY0sgiQQqNFKSY2dflaaRWotUh4af/CEBhxep/l5yjqQUXH1GFP1a3bK7EgA2uZqgGJB1luSrvOFizVbu9Q3eJwp5pmmoFa9yVnFyp9tw/GW8wtmvx42Dm38ufiIDpW2auuqfU2gClnAffGaD5UlXlAkmMNCE0SQY6Qn4NsKqALl3MO5qEZxGPEzUKBwfWUUsCYAAAAASUVORK5CYII="
          alt="gopay1264" class="pembayaran-lakukanpembayaran249-image4" />
        <img alt="Vector265"
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjknIGhlaWdodD0nMjknIHZpZXdCb3g9JzAgMCAyOSAyOScgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPHBhdGggZD0nTTE0LjUgMEM2LjUwNDcgMCAwIDYuNTA0NyAwIDE0LjVDMCAyMi40OTUzIDYuNTA0NyAyOSAxNC41IDI5QzIyLjQ5NTMgMjkgMjkgMjIuNDk1MyAyOSAxNC41QzI5IDYuNTA0NyAyMi40OTUzIDAgMTQuNSAwWk0xNS45NSAyMy4ySDEzLjA1VjIwLjNIMTUuOTVWMjMuMlpNMTcuMzY1MiAxNi4xMTY4QzE3LjA4MSAxNi4zNDU5IDE2LjgwNyAxNi41NjQ4IDE2LjU4OTUgMTYuNzgyM0MxNS45OTc5IDE3LjM3MjUgMTUuOTUxNSAxNy45MDkgMTUuOTUgMTcuOTMyMlYxOC4xMjVIMTMuMDVWMTcuODgyOUMxMy4wNSAxNy43MTE4IDEzLjA5MjEgMTYuMTc2MiAxNC41Mzc3IDE0LjczMDZDMTQuODIwNSAxNC40NDc4IDE1LjE3MTQgMTQuMTYwNyAxNS41Mzk3IDEzLjg2MkMxNi42MDQgMTIuOTk5MyAxNy4zMDI5IDEyLjM2OTkgMTcuMzAyOSAxMS41MDI4QzE3LjI4NiAxMC43NzAzIDE2Ljk4MzEgMTAuMDczNSAxNi40NTkgOS41NjE0N0MxNS45MzUgOS4wNDk0MyAxNS4yMzEzIDguNzYyODQgMTQuNDk4NiA4Ljc2MzAzQzEzLjc2NTkgOC43NjMyMiAxMy4wNjIzIDkuMDUwMTcgMTIuNTM4NSA5LjU2MjQ5QzEyLjAxNDcgMTAuMDc0OCAxMS43MTIyIDEwLjc3MTggMTEuNjk1NyAxMS41MDQzSDguNzk1N0M4Ljc5NTcgOC4zNTkyNSAxMS4zNTUgNS44IDE0LjUgNS44QzE3LjY0NTEgNS44IDIwLjIwNDMgOC4zNTkyNSAyMC4yMDQzIDExLjUwNDNDMjAuMjA0MyAxMy44MiAxOC40OTQ4IDE1LjIwMTggMTcuMzY1MiAxNi4xMTY4VjE2LjExNjhaJyBmaWxsPScjRUVFRUVFJy8+Cjwvc3ZnPgo="
          class="pembayaran-lakukanpembayaran249-svg1" />
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjkxJyBoZWlnaHQ9JzYyJyB2aWV3Qm94PScwIDAgMjkxIDYyJyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8cmVjdCB4PScwLjUnIHk9JzAuNScgd2lkdGg9JzI5MCcgaGVpZ2h0PSc2MScgcng9JzQuNScgZmlsbD0nd2hpdGUnIHN0cm9rZT0nI0U0RTRFNCcvPgo8L3N2Zz4K"
          alt="Rectangle252266" class="pembayaran-lakukanpembayaran249-image5" />
        <span class="pembayaran-lakukanpembayaran249-text04">
          <span class="pembayaran-lakukanpembayaran249-text05">
            <span>PT MotoBuddies Tbk</span>
            <br />
            <span></span>
          </span>
          <span class="pembayaran-lakukanpembayaran249-text09">
            0811 2222 3333
          </span>
        </span>
        <span class="pembayaran-lakukanpembayaran249-text10">Kirim ke:</span>
        <span class="pembayaran-lakukanpembayaran249-text11">
          <span>* Kirim hingga 3 digit terakhir.</span>
          <br />
          <span>
            ** Jangan khawatir sebab biaya kode unik akan dikembalikan
            sepenuhnya setelah transaksi berhasil
          </span>
        </span>
        <span class="pembayaran-lakukanpembayaran249-text15">Nominal:</span>
        <span class="pembayaran-lakukanpembayaran249-text16">
          <span class="pembayaran-lakukanpembayaran249-text17">
            Rp 7.850.
          </span>
          <span class="pembayaran-lakukanpembayaran249-text18">054</span>
        </span>
        <div class="pembayaran-lakukanpembayaran249-group1205272">
          <span class="pembayaran-lakukanpembayaran249-text19">
            Tampilkan QRIS
          </span>
          <img alt="Vector275"
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTMnIGhlaWdodD0nMTMnIHZpZXdCb3g9JzAgMCAxMyAxMycgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPHBhdGggZD0nTTAgNS43Nzc3OFYwSDUuNzc3NzhWNS43Nzc3OEgwWk0yLjE2NjY3IDIuMTY2NjdWMy42MTExMUgzLjYxMTExVjIuMTY2NjdIMi4xNjY2N1pNNy4yMjIyMiA1Ljc3Nzc4VjBIMTNWNS43Nzc3OEg3LjIyMjIyWk05LjM4ODg5IDIuMTY2NjdWMy42MTExMUgxMC44MzMzVjIuMTY2NjdIOS4zODg4OVpNMCA3LjIyMjIyVjEzSDUuNzc3NzhWNy4yMjIyMkgwWk0zLjYxMTExIDkuMzg4ODlWMTAuODMzM0gyLjE2NjY3VjkuMzg4ODlIMy42MTExMVpNOS4zODg4OSA3LjIyMjIySDcuMjIyMjJWOC42NjY2N0g5LjM4ODg5VjExLjU1NTZIOC42NjY2N1YxMC4xMTExSDcuMjIyMjJWMTNIMTAuMTExMVYxMS41NTU2SDExLjU1NTZWMTNIMTNWMTAuODMzM0gxMS41NTU2VjEwLjExMTFIMTAuODMzM1Y5LjM4ODg5SDEzVjcuMjIyMjJIMTAuODMzM1Y4LjY2NjY3SDkuMzg4ODlWNy4yMjIyMlonIGZpbGw9J3doaXRlJy8+Cjwvc3ZnPgo="
            class="pembayaran-lakukanpembayaran249-svg2" />
        </div>
        <a href="/motobuddies/payment-1">
        <div class="pembayaran-lakukanpembayaran249-group1202276">
          <span class="pembayaran-lakukanpembayaran249-text20">
            Ganti metode pembayaran
          </span>
        </div>
        </a>
        <span class="pembayaran-lakukanpembayaran249-text21">
          Mengalami masalah?
        </span>
        <span class="pembayaran-lakukanpembayaran249-text22">atau</span>
        <span class="pembayaran-lakukanpembayaran249-text23">
          <span class="pembayaran-lakukanpembayaran249-text24">Buka</span>
          <span class="pembayaran-lakukanpembayaran249-text25">bantuan</span>
        </span>
        <a href="payment-3" class="pembayaran-lakukanpembayaran249-button">
            Saya sudah bayar
        </a>

        <img src="/images/logo.png" alt="image" class="pembayaran-lakukanpembayaran249-image6" />
      </div>
      <img src="/images/menu icon.png" alt="image" class="pembayaran-lakukanpembayaran249-image7" />
    </div>
  </div>
</body>

</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/khalish.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Submit Question</title>
</head>
<body>
      <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark p-4" id="bg-coll">
          <h5 class="text-white h4">Collapsed content</h5>
          <span class="text-muted">Toggleable via the navbar brand.</span>
        </div>
      </div>
      <nav class="navbar navbar-dark bg-dark" id="navbar">
        <div class="container-fluid" id="conf-nav">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation" id="toggler-nav">
            <span class="navbar-toggler-icon" id="icon-nav"></span>
          </button>
          <a href="page3.html"><img src="/images/logo-mb.png" alt="" id="logo-mb"></a>
          <a href="page3.html"><img src="/images/logo-prof.png" alt="" id="logo-prof"></a>
        </div>
      </nav>
      <div class="container-fluid" id="conf-strip-oren">
        <img src="/images/strip-oren.png" alt="" id="strip-oren">
      </div>

    <div class="container">
        <h2 id="h-qa">Ajukan Pertanyaan Anda</h2>

        <form action="{{ route('checkLogin') }}" method="post">
            <div class="form-group">
                <textarea class="form-control" id="text-ask" placeholder="Tulis pertanyaan anda..."></textarea>
              </div>
            <button type="submit" class="btn btn-primary" id="btn-qa">Submit</button>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

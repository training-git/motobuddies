<nav>
  <input type="checkbox">
  <div class="nav-menu">
    <div class="menu-container">
      <a href="/motobuddies/homepage">Beranda</a>
      <a href="/motobuddies/riwayat">Riwayat Pemesanan</a>
      <a href="/motobuddies/question">Customer Service</a>
    </div>
    <div class="sign-out-container">
      <a href="{{ route('logout') }}">Sign Out</a>
    </div>
  </div>
  <div class="nav-container">  
    <div class="nav-hamburger">
      <span class="line1"></span>
      <span class="line2"></span>
      <span class="line3"></span>
    </div>
    <img src="/images/logo-motobuddies.svg" alt="">
    <a href="/motobuddies/profile"><img src="/images/user-icon.svg" alt="" width="42px"></a>
  </div>
</nav>
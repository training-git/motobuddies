<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/khalish.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Riwayat Transaksi</title>
</head>
<body>
  <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-dark p-4" id="bg-coll">
      <h5 class="text-white h4">Collapsed content</h5>
      <span class="text-muted">Toggleable via the navbar brand.</span>
    </div>
  </div>
  <nav class="navbar navbar-dark bg-dark" id="navbar">
    <div class="container-fluid" id="conf-nav">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation" id="toggler-nav">
        <span class="navbar-toggler-icon" id="icon-nav"></span>
      </button>
      <a href="page3.html"><img src="/images/logo-mb.png" alt="" id="logo-mb"></a>
      <a href="page3.html"><img src="/images/logo-prof.png" alt="" id="logo-prof"></a>
    </div>
  </nav>
  <div class="container-fluid" id="conf-strip-oren">
    <img src="/images/strip-oren.png" alt="" id="strip-oren">
  </div>

    <div class="container">
        <h2 id="h-rt">Riwayat Transaksi</h2>

        <form class="d-flex" id="pre-form-src">
            <input class="form-control me-2" type="search" placeholder="Cari transaksi" aria-label="Search" id="form-src">
        </form>

        <div class="card" id="card-rt">
            <div class="card-body">
              <p class="card-text" id="tipe-rt">Tipe : Servis</p>
              <img src="/images/tune-up.png" alt="" style="float: left;">
              <h5 class="card-title" id="title-rt">Servis tune up rutin</h5>
              <p class="card-text" id="time-rt">26 Maret 2022 | 09.39 WIB</p>
              <p class="card-text" id="harga-rt">Rp 80.000</p>
              <button type="submit" class="btn btn-primary" id="btn-rt-u">Ulasan</button>
            </div>
        </div>
        <div class="card" id="card-rt">
          <div class="card-body">
            <p class="card-text" id="tipe-rt">Tipe : Produk</p>
            <img src="/images/spion.png" alt="" style="float: left;">
            <h5 class="card-title" id="title-rt">Spion Motor SP51c</h5>
            <p class="card-text" id="time-rt">26 Maret 2022 | 09.52 WIB</p>
            <p class="card-text" id="harga-rt">Rp 63.000</p>
            <button type="submit" class="btn btn-primary" id="btn-rt-u">Ulasan</button>
          </div>
        </div>
      <div class="card" id="card-rt">
        <div class="card-body">
          <p class="card-text" id="tipe-rt">Tipe : Servis</p>
          <img src="/images/tune-up.png" alt="" style="float: left;">
          <h5 class="card-title" id="title-rt">Servis tune up rutin</h5>
          <p class="card-text" id="time-rt">26 Maret 2022 | 09.39 WIB</p>
          <p class="card-text" id="harga-rt">Rp 80.000</p>
          <button type="submit" class="btn btn-primary" id="btn-rt-u">Ulasan</button>
        </div>
      </div>       
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

@include('_head')
  <div class="container">
    @include('_nav')
    <div class="products-page-container">
      <h3>Produk</h3>
      <div class="search-bar">
        <form action="" method="GET">
          <i class="fa-solid fa-magnifying-glass"></i>
          <input type="text" class="search-input" placeholder="Cari produk">
        </form>
      </div>
      <div class="products-container">
        <h3>Motor</h3>
        <div class="products-motorcycle">
          {{-- Product looping --}}
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-1.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Spion SCP03x</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp80.000</h4>
            </div>
          </div>
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-1.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Spion SCP03x</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp80.000</h4>
            </div>
          </div>
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-1.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Spion SCP03x</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp80.000</h4>
            </div>
          </div>
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-1.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Spion SCP03x</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp80.000</h4>
            </div>
          </div>
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-1.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Spion SCP03x</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp80.000</h4>
            </div>
          </div>
          {{-- end of Product looping --}}
        </div>
        <h3>Mobil</h3>
        <div class="products-car">
          {{-- Product looping --}}
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-2.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Karet Stir Mobil K21</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp120.000</h4>
            </div>
          </div>
          <div class="product-card">
            <div class="product-image-container">
              <img src="/images/product-2.png" alt="">
            </div>
            <div class="product-detail-container">
              <h5 class="product-name">Karet Stir Mobil K21</h5>
              <span class="product-rating">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
              </span>
              <h4 class="product-price">Rp120.000</h4>
            </div>
          </div>
          {{-- end of Product looping --}}
      </div>
    </div>
  </div>
  @include('_footer')

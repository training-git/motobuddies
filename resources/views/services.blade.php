@include('_head')
  <div class="container">
    @include('_nav')
    <div class="services-page-container">
      <div class="recommendation">
        <div class="head-recommend">
          <h5>Rekomendasi Servis</h5>
          <i class="fa-solid fa-circle-info"></i>
        </div>
        <div class="body-recommend">
          <p>Anda telah melakukan servis <strong>Tune Up Rutin</strong> pada kilometer <strong>15219.4</strong></p>
          <p>Kami merekomendasikan Anda untuk melakukan servis <strong>Tune Up Rutin</strong> kembali pada kilometer <strong>30219.4</strong></p>
        </div>
      </div>
      <h3>Servis</h3>
      <div class="search-bar">
        <form action="" method="GET">
          <i class="fa-solid fa-magnifying-glass"></i>
          <input type="text" class="search-input" placeholder="Cari servis">
        </form>
      </div>
      <div class="services-container">
        <h3>Motor</h3>
        <div class="services-motorcycle">
          <div class="service-card">
            <span class="image-gradient-service"></span>
            <img src="{{ asset('images/service-1.png') }}" alt="">
            <div class="service-info">
              <div class="service-name">
                <h5>Tune Up Rutin</h5>
              </div>
              <div class="service-price">
                <p>Harga mulai</p>
                <h5>Rp80.000</h5>
              </div>
            </div>
            <a href="/motobuddies/booking" class="service-btn"><span>Pesan</span></a>
          </div>
          <div class="service-card">
            <span class="image-gradient-service"></span>
            <img src="{{ asset('images/service-2.png') }}" alt="">
            <div class="service-info">
              <div class="service-name">
                <h5>Tune Up Rutin</h5>
              </div>
              <div class="service-price">
                <p>Harga mulai</p>
                <h5>Rp80.000</h5>
              </div>
            </div>
            <a href="/motobuddies/booking" class="service-btn"><span>Pesan</span></a>
          </div>
          <div class="service-card">
            <span class="image-gradient-service"></span>
            <img src="{{ asset('images/service-3.png') }}" alt="">
            <div class="service-info">
              <div class="service-name">
                <h5>Tune Up Rutin</h5>
              </div>
              <div class="service-price">
                <p>Harga mulai</p>
                <h5>Rp80.000</h5>
              </div>
            </div>
            <a href="/motobuddies/booking" class="service-btn"><span>Pesan</span></a>
          </div>
        </div>
        <h3>Mobil</h3>
        <div class="services-car">
          <div class="service-unavailable">
            <p>Maaf, saat ini layanan servis mobil <span class="danger">belum tersedia</span> di daerah Anda.</p>
            <h6>Daerah Anda: <strong>Yogyakarta</strong></h6>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('_footer')

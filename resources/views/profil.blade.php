<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/andy.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>profil</title>
</head>
<body>
    <div class="container">
        @include('_nav')
        <h1 >Profile </h1>

        <div class="row ">
            <div class=" fotoprof col-3  mb-4">
                <img src="/images/foto profil.png" alt="foto profil mahasiswa" width="81" class="rounded-circle img-thumbnail" height="77" >
            </div>

            <div class="col-6 ">
                <ul class="list-group list-group-flush mt-4">
                    <li class="list-group-item">{{Auth::user()->name}}</li>
                    
                   
                  </ul>
            </div>
          
          </div>
          <div class="row ">
            <div class=" col-3  mb-4">
                <tr class="list1">
                    <td>Email </td>
                    <td>Alamat </td>
                    <td>Telepon </td>
                  </tr>
            </div>

            <div class="col-6">
                <tr>
                    <td>{{Auth::user()->email}}</td>
                    <td>{{Auth::user()->address}}</td>
                    <td>{{Auth::user()->number_phone}}</td>
                  </tr>
            </div>
            
          </div>
          <section class="but text-right">
            <a href="editprofile" class="btn btn-outline-orange" ><font color="#FF7A4"">Edit</font></a>
        </section>
    </div>
</body>
</html>
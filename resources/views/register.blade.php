<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/adam.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Register</title>
</head>
<body>
    <div class="container">
        <h1 >Sign Up</h1>

        <form action="{{ route('storeRegister') }}" method="post" id="register">
            @csrf
            <div class="form-group">
              <input type="email" name="email" class="form-control"  aria-describedby="emailHelp" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Kata Sandi">
            </div>
            <div class="form-group">
                <input type="text" name="address" class="form-control" placeholder="Alamat">
            </div>
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
            </div>
            <div class="form-group">
                <input type="text" name="phone_number" class="form-control" placeholder="Nomor Telepon">
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label">Saya setuju dengan kebijakan privasi</label>
            </div>
            <button type="submit" class="btn btn-primary">Buat Akun</button>
        </form>
        <p>Sudah memiliki akun? <a href="login">Sign in </a>

    </div>
</body>
</html>
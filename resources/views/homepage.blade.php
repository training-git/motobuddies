@include('_head')
<div class="container">
@include('_nav')
  <div class="greeting">
    <h5>Selamat Datang</h5>
    <h2>{{Auth::user()->name}}</h2>
  </div>

  <div class="articles">
    <h5>Artikel</h5>
    <div class="articles-container">
      <div class="article">
        <a href="#">
          <span class="image-gradient"></span>
          <img src="{{ asset('images/image-1.png') }}" alt="" class="article-image">
          <h5 class="article-title">Rawat Mobil itu Mudah.</h5>
          <span class="article-read-more">more >></span>
        </a>
      </div>
      <div class="article">
        <a href="#">
          <span class="image-gradient"></span>
          <img src="{{ asset('images/image-1.png') }}" alt="" class="article-image">
          <h5 class="article-title">Rawat Mobil itu Mudah.</h5>
          <span class="article-read-more">more >></span>
        </a>
      </div>
      <div class="article">
        <a href="#">
          <span class="image-gradient"></span>
          <img src="{{ asset('images/image-1.png') }}" alt="" class="article-image">
          <h5 class="article-title">Rawat Mobil itu Mudah.</h5>
          <span class="article-read-more">more >></span>
        </a>
      </div>
    </div>
  </div>

  <div class="services">
    <h5>Layanan</h5>
    <div class="services-container">
      <div class="service">
        <a href="services">
          <div class="service-icon-container">
            <img src="{{ asset('images/service-icon.svg') }}" alt="" class="service-icon">
          </div>
          <h6 class="service-name">Servis</h6>
        </a>
      </div>
      <div class="product">
        <a href="products">
          <div class="product-icon-container">
            <img src="{{ asset('images/product-icon.svg') }}" alt="" class="product-icon">
          </div>
          <h6 class="service-name">Produk</h6>
        </a>
      </div>
      <div class="faq">
      <a href="faq">
        <div class="faq-icon-container">
          <img src="{{ asset('images/faq-icon.svg') }}" alt="" class="faq-icon">
        </div>
        <h6 class="service-name">FAQ</h6>
      </a>
      </div>
    </div>
  </div>

  <div class="promos">
    <h5>Promo</h5>
    <div class="promos-container">
      <div class="promo">
        <div class="promo-image-container">
          <img src="{{ asset('images/promo-bca.png') }}" alt="" class="promo-image">
        </div>
        <p class="promo-text">Dapatkan voucher diskon 30% dengan pembayaran menggunakan BCA!</p>
      </div>
      <div class="promo">
        <div class="promo-image-container">
          <img src="{{ asset('images/promo-ovo.png') }}" alt="" class="promo-image">
        </div>
        <p class="promo-text">Dapatkan cashback total Rp 10.000 dengan pembayaran menggunakan OVO!</p>
      </div>
    </div>
  </div>
</div>
@include('_footer')
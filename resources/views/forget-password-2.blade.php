<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/adam.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Change Password</title>
</head>
<body>
    <div class="container">
        <h1 >Forget Password</h1>

        <img src="images/image 1.png" alt="" style="margin-top:64px; margin-bottom:64px;">
        <form action="{{route('storePassword')}}" method="post">
            @csrf
            <input type="hidden" name="email" value="{{ $user->email }}">
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="New Password">
            </div>
            <div class="form-group">
                <input type="password" name="retype" class="form-control" placeholder="Re-type New Password">
              </div>
            <button type="submit" class="btn btn-primary">Submmit</button>
        </form>
    </div>
</body>
</html>
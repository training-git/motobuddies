<!DOCTYPE html>

<html lang="english">

<head>
  <title>Pembayaran Berhasil</title>
  <meta property="twitter:card" content="summary_large_image" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta charset="utf-8" />
  <meta property="twitter:card" content="summary_large_image" />
  <style data-tag="reset-style-sheet">
    html {
      line-height: 1.15;
    }

    body {
      margin: 0;
    }

    * {
      box-sizing: border-box;
      border-width: 0;
      border-style: solid;
    }

    p,
    li,
    ul,
    pre,
    div,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    button,
    input,
    optgroup,
    select,
    textarea {
      font-family: inherit;
      font-size: 100%;
      line-height: 1.15;
      margin: 0;
    }

    button,
    select {
      text-transform: none;
    }

    button,
    [type="button"],
    [type="reset"],
    [type="submit"] {
      -webkit-appearance: button;
    }

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
      border-style: none;
      padding: 0;
    }

    button:-moz-focus,
    [type="button"]:-moz-focus,
    [type="reset"]:-moz-focus,
    [type="submit"]:-moz-focus {
      outline: 1px dotted ButtonText;
    }

    a {
      color: inherit;
      text-decoration: inherit;
    }

    input {
      padding: 2px 4px;
    }

    img {
      display: block;
    }
  </style>
  <style data-tag="default-style-sheet">
    html {
      font-family: Inter;
      font-size: 16px;
    }

    body {
      font-weight: 400;
      font-style: normal;
      text-decoration: none;
      text-transform: none;
      letter-spacing: normal;
      line-height: 1.15;
      color: var(--dl-color-gray-black);
      background-color: var(--dl-color-gray-white);

    }
  </style>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
    data-tag="font" />
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    data-tag="font" />
</head>

<body>
  <div>
    <link href="/css/payment_stylesheet.css" rel="stylesheet" />

    <div class="pembayaran-berhasil282-container">
      <div class="pembayaran-berhasil282-pembayaran-berhasil282">
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDI4JyBoZWlnaHQ9JzEwMScgdmlld0JveD0nMCAwIDQyOCAxMDEnIGZpbGw9J25vbmUnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+CjxyZWN0IHdpZHRoPSc0MjknIGhlaWdodD0nMTAxJyBmaWxsPScjMjIyMjIyJy8+Cjwvc3ZnPgo="
          alt="Rectangle172283" class="pembayaran-berhasil282-image" />
        <div class="pembayaran-berhasil282-group1179288">
          <img alt="Ellipse9289"
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDInIGhlaWdodD0nNDQnIHZpZXdCb3g9JzAgMCA0MiA0NCcgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPGVsbGlwc2UgY3g9JzIxJyBjeT0nMjInIHJ4PScyMScgcnk9JzIyJyBmaWxsPScjRkY3QTQ1Jy8+Cjwvc3ZnPgo="
            class="pembayaran-berhasil282-svg" />
          <img
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTYnIGhlaWdodD0nMTgnIHZpZXdCb3g9JzAgMCAxNiAxOCcgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPHBhdGggZD0nTTE0LjI5OTkgMTYuMzMzM1YxNC43MDM3QzE0LjI5OTkgMTMuODM5MiAxMy45NjggMTMuMDEwMiAxMy4zNzczIDEyLjM5OUMxMi43ODY1IDExLjc4NzggMTEuOTg1MyAxMS40NDQ0IDExLjE0OTkgMTEuNDQ0NEg0Ljg0OTg5QzQuMDE0NDYgMTEuNDQ0NCAzLjIxMzI0IDExLjc4NzggMi42MjI1IDEyLjM5OUMyLjAzMTc2IDEzLjAxMDIgMS42OTk4OSAxMy44MzkyIDEuNjk5ODkgMTQuNzAzN1YxNi4zMzMzJyBzdHJva2U9JyNGOUY5RjknIHN0cm9rZS13aWR0aD0nMicgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbGluZWpvaW49J3JvdW5kJy8+CjxwYXRoIGQ9J004LjAwMDAxIDguMTg1MTRDOS43Mzk3IDguMTg1MTQgMTEuMTUgNi43MjU5MiAxMS4xNSA0LjkyNTg5QzExLjE1IDMuMTI1ODUgOS43Mzk3IDEuNjY2NjMgOC4wMDAwMSAxLjY2NjYzQzYuMjYwMzEgMS42NjY2MyA0Ljg1MDAxIDMuMTI1ODUgNC44NTAwMSA0LjkyNTg5QzQuODUwMDEgNi43MjU5MiA2LjI2MDMxIDguMTg1MTQgOC4wMDAwMSA4LjE4NTE0Wicgc3Ryb2tlPScjRjlGOUY5JyBzdHJva2Utd2lkdGg9JzInIHN0cm9rZS1saW5lY2FwPSdyb3VuZCcgc3Ryb2tlLWxpbmVqb2luPSdyb3VuZCcvPgo8L3N2Zz4K"
            alt="Group50290" class="pembayaran-berhasil282-image1" />
        </div>
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDI4JyBoZWlnaHQ9JzMwJyB2aWV3Qm94PScwIDAgNDI4IDMwJyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8cmVjdCB3aWR0aD0nNDI5JyBoZWlnaHQ9JzMwJyBmaWxsPScjRkY3QTQ1Jy8+Cjwvc3ZnPgo="
          alt="Rectangle173294" class="pembayaran-berhasil282-image2" />
        <span class="pembayaran-berhasil282-text">Transaksi Berhasil !</span>
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTYwJyBoZWlnaHQ9JzE2MCcgdmlld0JveD0nMCAwIDE2MCAxNjAnIGZpbGw9J25vbmUnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+CjxnIGZpbHRlcj0ndXJsKCNmaWx0ZXIwX2lfMl8xMjQpJz4KPHBhdGggZD0nTTUxLjI1IDgzLjY4NzVMNzEuMzI2NCAxMDMuNzY0QzcyLjE1OCAxMDQuNTk1IDczLjUyNDIgMTA0LjUzNCA3NC4yNzcgMTAzLjYzTDEwOC45MTcgNjIuMDYyNScgc3Ryb2tlPScjMzFDRjIzJyBzdHJva2Utd2lkdGg9JzE1JyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1saW5lam9pbj0ncm91bmQnIHN0eWxlPSdtaXgtYmxlbmQtbW9kZTptdWx0aXBseScvPgo8cGF0aCBkPSdNODAuMDgzMyAxNTIuMTY3QzExOS44OTQgMTUyLjE2NyAxNTIuMTY3IDExOS44OTQgMTUyLjE2NyA4MC4wODMzQzE1Mi4xNjcgNDAuMjcyOCAxMTkuODk0IDggODAuMDgzMyA4QzQwLjI3MjggOCA4IDQwLjI3MjggOCA4MC4wODMzQzggMTE5Ljg5NCA0MC4yNzI4IDE1Mi4xNjcgODAuMDgzMyAxNTIuMTY3Wicgc3Ryb2tlPScjMzFDRjIzJyBzdHJva2Utd2lkdGg9JzE1JyBzdHlsZT0nbWl4LWJsZW5kLW1vZGU6bXVsdGlwbHknLz4KPC9nPgo8ZGVmcz4KPGZpbHRlciBpZD0nZmlsdGVyMF9pXzJfMTI0JyB4PScwLjUnIHk9JzAuNScgd2lkdGg9JzE1OS4xNjcnIGhlaWdodD0nMTYzLjE2NycgZmlsdGVyVW5pdHM9J3VzZXJTcGFjZU9uVXNlJyBjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM9J3NSR0InPgo8ZmVGbG9vZCBmbG9vZC1vcGFjaXR5PScwJyByZXN1bHQ9J0JhY2tncm91bmRJbWFnZUZpeCcvPgo8ZmVCbGVuZCBtb2RlPSdub3JtYWwnIGluPSdTb3VyY2VHcmFwaGljJyBpbjI9J0JhY2tncm91bmRJbWFnZUZpeCcgcmVzdWx0PSdzaGFwZScvPgo8ZmVDb2xvck1hdHJpeCBpbj0nU291cmNlQWxwaGEnIHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCcgcmVzdWx0PSdoYXJkQWxwaGEnLz4KPGZlT2Zmc2V0IGR5PSc0Jy8+CjxmZUdhdXNzaWFuQmx1ciBzdGREZXZpYXRpb249JzInLz4KPGZlQ29tcG9zaXRlIGluMj0naGFyZEFscGhhJyBvcGVyYXRvcj0nYXJpdGhtZXRpYycgazI9Jy0xJyBrMz0nMScvPgo8ZmVDb2xvck1hdHJpeCB0eXBlPSdtYXRyaXgnIHZhbHVlcz0nMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMC4yNSAwJy8+CjxmZUJsZW5kIG1vZGU9J25vcm1hbCcgaW4yPSdzaGFwZScgcmVzdWx0PSdlZmZlY3QxX2lubmVyU2hhZG93XzJfMTI0Jy8+CjwvZmlsdGVyPgo8L2RlZnM+Cjwvc3ZnPgo="
          alt="Group2124" class="pembayaran-berhasil282-image3" />
        <a href="/motobuddies/review/1"><img
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTcyJyBoZWlnaHQ9JzU0JyB2aWV3Qm94PScwIDAgMTcyIDU0JyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8ZyBmaWx0ZXI9J3VybCgjZmlsdGVyMF9kXzJfMTI3KSc+CjxyZWN0IHg9JzQnIHdpZHRoPScxNjQnIGhlaWdodD0nNDYnIHJ4PSc1JyBmaWxsPScjMzFDRDIzJy8+CjwvZz4KPGRlZnM+CjxmaWx0ZXIgaWQ9J2ZpbHRlcjBfZF8yXzEyNycgeD0nMCcgeT0nMCcgd2lkdGg9JzE3MicgaGVpZ2h0PSc1NCcgZmlsdGVyVW5pdHM9J3VzZXJTcGFjZU9uVXNlJyBjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM9J3NSR0InPgo8ZmVGbG9vZCBmbG9vZC1vcGFjaXR5PScwJyByZXN1bHQ9J0JhY2tncm91bmRJbWFnZUZpeCcvPgo8ZmVDb2xvck1hdHJpeCBpbj0nU291cmNlQWxwaGEnIHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCcgcmVzdWx0PSdoYXJkQWxwaGEnLz4KPGZlT2Zmc2V0IGR5PSc0Jy8+CjxmZUdhdXNzaWFuQmx1ciBzdGREZXZpYXRpb249JzInLz4KPGZlQ29tcG9zaXRlIGluMj0naGFyZEFscGhhJyBvcGVyYXRvcj0nb3V0Jy8+CjxmZUNvbG9yTWF0cml4IHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwLjI1IDAnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbjI9J0JhY2tncm91bmRJbWFnZUZpeCcgcmVzdWx0PSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl8xMjcnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbj0nU291cmNlR3JhcGhpYycgaW4yPSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl8xMjcnIHJlc3VsdD0nc2hhcGUnLz4KPC9maWx0ZXI+CjwvZGVmcz4KPC9zdmc+Cg=="
            alt="Rectangle2502127" class="pembayaran-berhasil282-image4" />
          <span class="pembayaran-berhasil282-text1">Beri ulasan</span></a>
        <a href="/motobuddies/homepage"><img
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTcyJyBoZWlnaHQ9JzU0JyB2aWV3Qm94PScwIDAgMTcyIDU0JyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8ZyBmaWx0ZXI9J3VybCgjZmlsdGVyMF9kXzJfMTI5KSc+CjxyZWN0IHg9JzQnIHdpZHRoPScxNjQnIGhlaWdodD0nNDYnIHJ4PSc1JyBmaWxsPScjRkY3QTQ1Jy8+CjwvZz4KPGRlZnM+CjxmaWx0ZXIgaWQ9J2ZpbHRlcjBfZF8yXzEyOScgeD0nMCcgeT0nMCcgd2lkdGg9JzE3MicgaGVpZ2h0PSc1NCcgZmlsdGVyVW5pdHM9J3VzZXJTcGFjZU9uVXNlJyBjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM9J3NSR0InPgo8ZmVGbG9vZCBmbG9vZC1vcGFjaXR5PScwJyByZXN1bHQ9J0JhY2tncm91bmRJbWFnZUZpeCcvPgo8ZmVDb2xvck1hdHJpeCBpbj0nU291cmNlQWxwaGEnIHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCcgcmVzdWx0PSdoYXJkQWxwaGEnLz4KPGZlT2Zmc2V0IGR5PSc0Jy8+CjxmZUdhdXNzaWFuQmx1ciBzdGREZXZpYXRpb249JzInLz4KPGZlQ29tcG9zaXRlIGluMj0naGFyZEFscGhhJyBvcGVyYXRvcj0nb3V0Jy8+CjxmZUNvbG9yTWF0cml4IHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwLjI1IDAnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbjI9J0JhY2tncm91bmRJbWFnZUZpeCcgcmVzdWx0PSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl8xMjknLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbj0nU291cmNlR3JhcGhpYycgaW4yPSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl8xMjknIHJlc3VsdD0nc2hhcGUnLz4KPC9maWx0ZXI+CjwvZGVmcz4KPC9zdmc+Cg=="
            alt="Rectangle2512129" class="pembayaran-berhasil282-image5" />
          <span class="pembayaran-berhasil282-text2">Home</span></a>
        <img src="/images/Invoice MotoBuddies.png" alt="image" class="pembayaran-berhasil282-image6" />
      </div>
      <img src="/images/logo.png" alt="image" class="pembayaran-berhasil282-image7" />
      <img src="/images/menu icon.png" alt="image" class="pembayaran-berhasil282-image8" />
    </div>
  </div>
</body>

</html>
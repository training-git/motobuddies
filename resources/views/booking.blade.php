@include('_head')
  <div class="container">
    @include('_nav')
    <div class="booking-page-container">
      <div class="booking-header">
        <span class="image-gradient-booking"></span>
        <img src="{{ asset('images/service-1.png') }}" alt="">
        <div class="service-info">
          <div class="service-name">
            <h5>Tune Up Rutin</h5>
          </div>
          <div class="service-price">
            <p>Harga mulai</p>
            <h5>Rp80.000</h5>
          </div>
        </div>
      </div>
      <div class="booking-time">
        <h5>Pilih tanggal dan waktu</h5>
        <form action="" id="datetime" method="get">
          <div class="date">
            <input type="datetime-local" id="flatpickr" name="datetime" placeholder="Silakan masukkan tanggal dan waktu">
          </div>
          <div class="buttons">
            <a href="" class="back-btn">Kembali</a>
            <a href="/motobuddies/payment-1" class="confirm-btn">Booking Sekarang</a>
          </div>
        </form>
      </div>
    </div>
  </div>
@include('_footer')

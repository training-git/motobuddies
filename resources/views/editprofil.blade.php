<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/andy.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Edit profil</title>
</head>
<body>
    <div class="container">
        @include('_nav')
        <h1 >Edit Data Diri</h1>
        
        <section class="jumbotron text-center">
                <img src="/images/foto profil.png" alt="foto profil mahasiswa" width="81" class="rounded-circle img-thumbnail" height="77" >
            </section>
        
         
        <div class="row "></div>
        <form action="/motobuddies/homepage" method="get">
            <div class="form-group">
              <input type="email" class="form-control"  aria-describedby="emailHelp" placeholder="Email" value="{{Auth::user()->email}}">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Nama" value="{{Auth::user()->name}}">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nomor Telepon" value="{{Auth::user()->phone_number}}">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Alamat" value="{{Auth::user()->address}}">
            </div>
            <button type="submit" class="btn btn-primary">Simpan Profil</button>
        </form>
    </div>
</div>
</body>
</html>
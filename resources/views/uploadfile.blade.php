<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/andy.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Upload Laporan</title>
</head>
<body>
    <div class="container">
        @include('_nav')
        <h1 >Upload Laporan</h1>

        <form action="" method="" id="register">
            <div class="form-group">
              <input type="text" class="form-control"  placeholder="Nama Dokumen">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Waktu Laporan dibuat">
            </div>
        </h4>
        <div class="ajax-upload-dragdrop upload-drag-and-drop-box">
          <img src="/images/Group 1223.png" alt="upload-file" />
          </br>
          
          <!-- <p class="file-type-desc">( <font color="red">*</font>file extension allowed: <strong>.pdf )</p> -->
           </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
       

    </div>
</body>
</html>
<!DOCTYPE html>

<html lang="english">

<head>
  <title>Pembayaran Gagal</title>
  <meta property="twitter:card" content="summary_large_image" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta charset="utf-8" />
  <meta property="twitter:card" content="summary_large_image" />
  <style data-tag="reset-style-sheet">
    html {
      line-height: 1.15;
    }

    body {
      margin: 0;
    }

    * {
      box-sizing: border-box;
      border-width: 0;
      border-style: solid;
    }

    p,
    li,
    ul,
    pre,
    div,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    button,
    input,
    optgroup,
    select,
    textarea {
      font-family: inherit;
      font-size: 100%;
      line-height: 1.15;
      margin: 0;
    }

    button,
    select {
      text-transform: none;
    }

    button,
    [type="button"],
    [type="reset"],
    [type="submit"] {
      -webkit-appearance: button;
    }

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
      border-style: none;
      padding: 0;
    }

    button:-moz-focus,
    [type="button"]:-moz-focus,
    [type="reset"]:-moz-focus,
    [type="submit"]:-moz-focus {
      outline: 1px dotted ButtonText;
    }

    a {
      color: inherit;
      text-decoration: inherit;
    }

    input {
      padding: 2px 4px;
    }

    img {
      display: block;
    }
  </style>
  <style data-tag="default-style-sheet">
    html {
      font-family: Inter;
      font-size: 16px;
    }

    body {
      font-weight: 400;
      font-style: normal;
      text-decoration: none;
      text-transform: none;
      letter-spacing: normal;
      line-height: 1.15;
      color: var(--dl-color-gray-black);
      background-color: var(--dl-color-gray-white);

    }
  </style>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
    data-tag="font" />
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    data-tag="font" />
</head>

<body>
  <div>
    <link href="/css/payment_stylesheet.css" rel="stylesheet" />

    <div class="pembayaran-gagal2131-container">
      <div class="pembayaran-gagal2131-pembayaran-gagal2131">
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDI4JyBoZWlnaHQ9JzEwMScgdmlld0JveD0nMCAwIDQyOCAxMDEnIGZpbGw9J25vbmUnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+CjxyZWN0IHdpZHRoPSc0MjknIGhlaWdodD0nMTAxJyBmaWxsPScjMjIyMjIyJy8+Cjwvc3ZnPgo="
          alt="Rectangle1722132" class="pembayaran-gagal2131-image" />
        <div class="pembayaran-gagal2131-group11792137">
          <img alt="Ellipse92138"
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDInIGhlaWdodD0nNDQnIHZpZXdCb3g9JzAgMCA0MiA0NCcgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPGVsbGlwc2UgY3g9JzIxJyBjeT0nMjInIHJ4PScyMScgcnk9JzIyJyBmaWxsPScjRkY3QTQ1Jy8+Cjwvc3ZnPgo="
            class="pembayaran-gagal2131-svg" />
          <img
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTYnIGhlaWdodD0nMTgnIHZpZXdCb3g9JzAgMCAxNiAxOCcgZmlsbD0nbm9uZScgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KPHBhdGggZD0nTTE0LjI5OTkgMTYuMzMzM1YxNC43MDM3QzE0LjI5OTkgMTMuODM5MiAxMy45NjggMTMuMDEwMiAxMy4zNzczIDEyLjM5OUMxMi43ODY1IDExLjc4NzggMTEuOTg1MyAxMS40NDQ0IDExLjE0OTkgMTEuNDQ0NEg0Ljg0OTg5QzQuMDE0NDYgMTEuNDQ0NCAzLjIxMzI0IDExLjc4NzggMi42MjI1IDEyLjM5OUMyLjAzMTc2IDEzLjAxMDIgMS42OTk4OSAxMy44MzkyIDEuNjk5ODkgMTQuNzAzN1YxNi4zMzMzJyBzdHJva2U9JyNGOUY5RjknIHN0cm9rZS13aWR0aD0nMicgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbGluZWpvaW49J3JvdW5kJy8+CjxwYXRoIGQ9J003Ljk5OTk4IDguMTg1MTRDOS43Mzk2NyA4LjE4NTE0IDExLjE1IDYuNzI1OTIgMTEuMTUgNC45MjU4OUMxMS4xNSAzLjEyNTg1IDkuNzM5NjcgMS42NjY2MyA3Ljk5OTk4IDEuNjY2NjNDNi4yNjAyOCAxLjY2NjYzIDQuODQ5OTggMy4xMjU4NSA0Ljg0OTk4IDQuOTI1ODlDNC44NDk5OCA2LjcyNTkyIDYuMjYwMjggOC4xODUxNCA3Ljk5OTk4IDguMTg1MTRaJyBzdHJva2U9JyNGOUY5RjknIHN0cm9rZS13aWR0aD0nMicgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbGluZWpvaW49J3JvdW5kJy8+Cjwvc3ZnPgo="
            alt="Group502139" class="pembayaran-gagal2131-image1" />
        </div>
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nNDI4JyBoZWlnaHQ9JzMwJyB2aWV3Qm94PScwIDAgNDI4IDMwJyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8cmVjdCB3aWR0aD0nNDI5JyBoZWlnaHQ9JzMwJyBmaWxsPScjRkY3QTQ1Jy8+Cjwvc3ZnPgo="
          alt="Rectangle1732143" class="pembayaran-gagal2131-image2" />
        <span class="pembayaran-gagal2131-text">Transaksi Gagal !</span>
        <span class="pembayaran-gagal2131-text01">
          <span class="pembayaran-gagal2131-text02">
            <span>Penyebab:</span>
            <br />
            <span></span>
          </span>
          <span class="pembayaran-gagal2131-text06">
            Waktu tunggu anda telah habis, sesi pembayaran telah hangus.
            Jangan khawatir, silahkan coba lagi
          </span>
        </span>
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTcyJyBoZWlnaHQ9JzU0JyB2aWV3Qm94PScwIDAgMTcyIDU0JyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPgo8ZyBmaWx0ZXI9J3VybCgjZmlsdGVyMF9kXzJfMTQ2KSc+CjxyZWN0IHg9JzQnIHdpZHRoPScxNjQnIGhlaWdodD0nNDYnIHJ4PSc1JyBmaWxsPScjMzFDRDIzJy8+CjwvZz4KPGRlZnM+CjxmaWx0ZXIgaWQ9J2ZpbHRlcjBfZF8yXzE0NicgeD0nMCcgeT0nMCcgd2lkdGg9JzE3MicgaGVpZ2h0PSc1NCcgZmlsdGVyVW5pdHM9J3VzZXJTcGFjZU9uVXNlJyBjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM9J3NSR0InPgo8ZmVGbG9vZCBmbG9vZC1vcGFjaXR5PScwJyByZXN1bHQ9J0JhY2tncm91bmRJbWFnZUZpeCcvPgo8ZmVDb2xvck1hdHJpeCBpbj0nU291cmNlQWxwaGEnIHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCcgcmVzdWx0PSdoYXJkQWxwaGEnLz4KPGZlT2Zmc2V0IGR5PSc0Jy8+CjxmZUdhdXNzaWFuQmx1ciBzdGREZXZpYXRpb249JzInLz4KPGZlQ29tcG9zaXRlIGluMj0naGFyZEFscGhhJyBvcGVyYXRvcj0nb3V0Jy8+CjxmZUNvbG9yTWF0cml4IHR5cGU9J21hdHJpeCcgdmFsdWVzPScwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwLjI1IDAnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbjI9J0JhY2tncm91bmRJbWFnZUZpeCcgcmVzdWx0PSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl8xNDYnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbj0nU291cmNlR3JhcGhpYycgaW4yPSdlZmZlY3QxX2Ryb3BTaGFkb3dfMl8xNDYnIHJlc3VsdD0nc2hhcGUnLz4KPC9maWx0ZXI+CjwvZGVmcz4KPC9zdmc+Cg=="
          alt="Rectangle2502146" class="pembayaran-gagal2131-image3" />
        <span class="pembayaran-gagal2131-text07">Ulangi</span>
        <img
          src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMTYwJyBoZWlnaHQ9JzE2MCcgdmlld0JveD0nMCAwIDE2MCAxNjAnIGZpbGw9J25vbmUnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc+CjxnIGZpbHRlcj0ndXJsKCNmaWx0ZXIwX2lfMl8xNDgpJz4KPHBhdGggZD0nTTEwMS43MSAxMDEuNzExTDU4LjQ1OTUgNTguNDU5NU0xMDEuNzEgNTguNDU5NUw1OC40NTk1IDEwMS43MTEnIHN0cm9rZT0nI0ZGNzAzNycgc3Ryb2tlLXdpZHRoPScxNScgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJy8+CjxwYXRoIGQ9J004MC4wODUgMTUyLjE3QzExOS44OTYgMTUyLjE3IDE1Mi4xNyAxMTkuODk2IDE1Mi4xNyA4MC4wODVDMTUyLjE3IDQwLjI3MzYgMTE5Ljg5NiA4IDgwLjA4NSA4QzQwLjI3MzYgOCA4IDQwLjI3MzYgOCA4MC4wODVDOCAxMTkuODk2IDQwLjI3MzYgMTUyLjE3IDgwLjA4NSAxNTIuMTdaJyBzdHJva2U9JyNGRjcwMzcnIHN0cm9rZS13aWR0aD0nMTUnLz4KPC9nPgo8ZGVmcz4KPGZpbHRlciBpZD0nZmlsdGVyMF9pXzJfMTQ4JyB4PScwLjUnIHk9JzAuNScgd2lkdGg9JzE1OS4xNycgaGVpZ2h0PScxNjMuMTcnIGZpbHRlclVuaXRzPSd1c2VyU3BhY2VPblVzZScgY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzPSdzUkdCJz4KPGZlRmxvb2QgZmxvb2Qtb3BhY2l0eT0nMCcgcmVzdWx0PSdCYWNrZ3JvdW5kSW1hZ2VGaXgnLz4KPGZlQmxlbmQgbW9kZT0nbm9ybWFsJyBpbj0nU291cmNlR3JhcGhpYycgaW4yPSdCYWNrZ3JvdW5kSW1hZ2VGaXgnIHJlc3VsdD0nc2hhcGUnLz4KPGZlQ29sb3JNYXRyaXggaW49J1NvdXJjZUFscGhhJyB0eXBlPSdtYXRyaXgnIHZhbHVlcz0nMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMTI3IDAnIHJlc3VsdD0naGFyZEFscGhhJy8+CjxmZU9mZnNldCBkeT0nNCcvPgo8ZmVHYXVzc2lhbkJsdXIgc3RkRGV2aWF0aW9uPScyJy8+CjxmZUNvbXBvc2l0ZSBpbjI9J2hhcmRBbHBoYScgb3BlcmF0b3I9J2FyaXRobWV0aWMnIGsyPSctMScgazM9JzEnLz4KPGZlQ29sb3JNYXRyaXggdHlwZT0nbWF0cml4JyB2YWx1ZXM9JzAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAuMjUgMCcvPgo8ZmVCbGVuZCBtb2RlPSdub3JtYWwnIGluMj0nc2hhcGUnIHJlc3VsdD0nZWZmZWN0MV9pbm5lclNoYWRvd18yXzE0OCcvPgo8L2ZpbHRlcj4KPC9kZWZzPgo8L3N2Zz4K"
          alt="Group2148" class="pembayaran-gagal2131-image4" />
        <div class="pembayaran-gagal2131-group12062151">
          <span class="pembayaran-gagal2131-text08">
            Ganti metode pembayaran
          </span>
        </div>
        <span class="pembayaran-gagal2131-text09">Mengalami masalah?</span>
        <span class="pembayaran-gagal2131-text10">atau</span>
        <span class="pembayaran-gagal2131-text11">
          <span class="pembayaran-gagal2131-text12">Buka</span>
          <span class="pembayaran-gagal2131-text13">bantuan</span>
        </span>
      </div>
      <img src="/images/logo.png" alt="image" class="pembayaran-gagal2131-image5" />
      <img src="/images/menu icon.png" alt="image" class="pembayaran-gagal2131-image6" />
    </div>
  </div>
</body>

</html>
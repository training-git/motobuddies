<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adam.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/andy.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Review</title>
</head>
<body>
    @include('_head')
    <div class="container">
    @include('_nav')
    <div class="isi">
        <div class="rating-header" style="margin-top: 100px">
            <h6>18/04/2022 - Service Motor Aerox</h6>

            <h4>Hai Adam! <br> Suka Dengan Layanan Kami?</h4>
        </div>
        <form action="{{ route('storeReview', $service->id_service) }}" method="post" class="rating">
            @csrf
            <input type="radio" value="1" name="rating" id="rating1">
            <label for="rating1" class="fa fa-star"></label>
            <input type="radio" value="2" name="rating" id="rating2">
            <label for="rating2" class="fa fa-star"></label>
            <input type="radio" value="3" name="rating" id="rating3">
            <label for="rating3" class="fa fa-star"></label>
            <input type="radio" value="4" name="rating" id="rating4">
            <label for="rating4" class="fa fa-star"></label>
            <input type="radio" value="5" name="rating" id="rating5">
            <label for="rating5" class="fa fa-star"></label>

            <div class="form-group">
                <textarea class="form-control" name="ulasan" rows="4" placeholder="Tulis komentar anda..."></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
    </div>
</body>
</html>
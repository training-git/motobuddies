<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/andy.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Laporanr</title>
</head>
<body>
    <div class="container">
        @include('_nav')
        <h1 >Lihat Laporan</h1>
        <a href="/motobuddies/uploadfile" class="service-btn" style="margin-left: 12px; margin-top : 24px;"><span>Add Report</span></a>
        <section class="down">
        <select name="Pilih Tahun" id="tah" class="drop">
            <option value="null">Pilih Tahun</option>
            <option value="2022">2022</option>
            <option value="2021">2021</option>
          </select>
        </section>
        <section class="down">
          <select name="Pilih Bulan" id="bul" class="drop">
            <option value="null">Pilih Bulan</option>
            <option value="Januari">Januari</option>
            <option value="Februari">Februari</option>
            <option value="Maret">Maret</option>
            <option value="April">April</option>
            <option value="Mei">Mei</option>
            <option value="Juni">Juni</option>
            <option value="Juli">Juli</option>
            <option value="Agustus">Agustus</option>
            <option value="September">September</option>
            <option value="Oktober">Oktober</option>
            <option value="November">November</option>
            <option value="Desember">Desember</option>
          </select>
        </section>
        <a href="/motobuddies/uploadfile" class="service-btn" style="margin-left: 12px; margin-bottom : 24px;"><span>Search Report</span></a>
        <h2> Hasil Laporan</h2>
        <div class="card" style="height: 16rem; width: 24rem;">
          <img src="/images/PDF2.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Laporan penjualan April 2022</h5>
          </div>
        </div>
    </div>
</body>
</html>
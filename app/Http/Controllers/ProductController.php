<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        return view('products', [
            "title" => "Produk",
            "products" => Product::all(),
        ]);
    }

    public function getProduct($id){
        return view('products', [
            "title" => "Produk",
            "products" => Product::find($id),
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;

class ServiceController extends Controller
{
    public function index(){
        return view('services', [
            "title" => "Servis",
            "services" => Service::all(),
        ]);
    }

    public function getService($id){
        return view('services', [
            "title" => "Servis",
            "services" => Service::find($id),
        ]);
    }

    public function bookService(){
        return view('booking', [
            "title" => "Booking Servis"
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(){
        return view('articles', [
            "title" => "Artikel",
            "articles" => Article::all(),
        ]);
    }

    public function getArticle($id){
        return view('articles', [
            "title" => "Artikel",
            "articles" => Article::find($id),
        ]);
    }
}

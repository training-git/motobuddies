<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function indexLogin()
    {
        return view('login');
    }

    public function indexRegister()
    {
        return view('register');
    }

    public function indexForgetPassword()
    {
        return view('forget-password');
    }

    public function indexChangePassword(Request $req)
    {
        $user = User::where('email', $req->email)->first();
        
        if(!empty($user)){
            return view('forget-password-2')->with(compact('user'));
        } else {
            return view('forget-password');
        }
        
    }

    public function indexHome()
    {
        return view('homepage');
    }

    public function checkLogin(Request $req)
    {
        $credentials = $req->only('email', 'password');

        if(Auth::attempt($credentials)) {
            if(Auth::user()->name != 'admin') {
                return redirect('/motobuddies/homepage');
            } else {
                return redirect('/motobuddies/laporan');
            }
            
        } else {
            return redirect('login');
        }
    }

    public function storeRegister(Request $req)
    {
        $user = new User;

        $user->email            = $req->email;
        $user->password         = bcrypt($req->password);
        $user->address          = $req->address;
        $user->name             = $req->name;
        $user->phone_number     = $req->phone_number;
        $user->vehicle_type     = 1;
        $user->age              = 20;
        $user->email_verified   = true;
        
        if($user->save()) return view('login');
    }

    public function storePassword(Request $req)
    {
        $user = User::where('email', $req->email)->first();
            
        if($req->password == $req->retype){
            $user->password         = bcrypt($req->password);

            if($user->save()) return view('login');
        } else {
            return view('forget-password-2')->with(compact('user'));
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function indexFAQ()
    {
        return view('faq');
    }

    public function storeQuestion()
    {
        return view('question');
    }

    public function indexRiwayat()
    {
        return view('riwayat');
    }

    public function indexProfil()
    {
        return view('profil');
    }

    public function storeEditProfil()
    {
        return view('editprofil');
    }

    public function storeUploadFile()
    {
        return view('uploadfile');
    }

    public function storeLaporan()
    {
        return view('laporan');
    }
}

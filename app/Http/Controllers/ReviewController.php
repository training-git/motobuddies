<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Service;
use App\Models\Review;

class ReviewController extends Controller
{
    public function indexReview($id)
    {
        $service = Service::where('id_service', $id)->first();
        return view('review')->with(compact('service'));
    }

    public function storeReview(Request $req, $id)
    {
        $review = new Review();
        $review->id_user = Auth::user()->id;
        $review->id_service = $id;
        $review->description = $req->ulasan;
        $review->tip = null;
        $review->rating = $req->rating;
        $review->save();

        return redirect('motobuddies/homepage');
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\payment;
use App\Http\Controllers\payment_1page;
use App\Http\Controllers\payment_2page;
use App\Http\Controllers\payment_3page;
use App\Http\Controllers\payment_4page;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [AuthController::class, 'indexLogin'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::get('register', [AuthController::class, 'indexRegister']);
Route::get('forget-password', [AuthController::class, 'indexForgetPassword']);
Route::post('change-password', [AuthController::class, 'indexChangePassword']);
Route::post('forget-password', [AuthController::class, 'storePassword'])->name('storePassword');
Route::post('login', [AuthController::class, 'checkLogin'])->name('checkLogin');
Route::post('register', [AuthController::class, 'storeRegister'])->name('storeRegister');
Route::get('motobuddies/faq', [AuthController::class, 'indexFAQ'])->name('indexFaq');
Route::get('motobuddies/question', [AuthController::class, 'storeQuestion'])->name('storeQuestion');
Route::get('motobuddies/riwayat', [AuthController::class, 'indexRiwayat'])->name('indexRiwayat');


Route::prefix('motobuddies')->middleware('auth')->group(function() {
    Route::get('homepage', [AuthController::class, 'indexHome']);
    Route::get('review/{id}', [ReviewController::class, 'indexReview']);
    Route::post('review/{id}', [ReviewController::class, 'storeReview'])->name('storeReview');
    Route::get('products', [ProductController::class, 'index']);
    Route::get('services', [ServiceController::class, 'index']);
    Route::get('booking', [ServiceController::class, 'bookService']);

    Route::get('payment-1', function(){
        return view('payment_1page');
    });
    Route::get('payment-2', function(){
        return view('payment_2page');
    });
    Route::get('payment-3', function(){
        return view('payment_3page');
    });
    Route::get('payment-4', function(){
        return view('payment_4page');
    });

    Route::get('profile', [AuthController::class, 'indexProfil'])->name('indexProfil');
    Route::get('editprofile', [AuthController::class, 'storeEditProfil'])->name('storeEditProfil');
    Route::get('uploadfile', [AuthController::class, 'storeUploadFile'])->name('storeUploadFile');
    Route::get('laporan', [AuthController::class, 'storeLaporan'])->name('storeLaporan');
});
